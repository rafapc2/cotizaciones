var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var establishmentSchema = new Schema({
    name: { type: String, required: [true, 'El	nombre	es	necesario'] },
    img: { type: String, required: false },
    user: { type: Schema.Types.ObjectId, ref: 'User' }
}, { collection: 'establishments' });

module.exports = mongoose.model('Establishment', establishmentSchema);