var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var medicalHistorySchema = new Schema({
    diagnostic: { type: String, required: true },
    treatment: { type: String, required: false },
    date: { type: Date, required: [true, 'La fecha de atención es requerida'] },
    pet: {
        type: Schema.Types.ObjectId,
        ref: 'Pet',
        required: [true, 'El id	de la mascota es un campo obligatorio ']
    },
    establishment: {
        type: Schema.Types.ObjectId,
        ref: 'Establishment',
        required: [true, 'El id	del establecimiento es un campo obligatorio ']
    },
    professional: {
        type: Schema.Types.ObjectId,
        ref: 'Professional',
        required: [true, 'el id del profesional es requerido']
    }
}, { collection: 'medicalHistories' });

module.exports = mongoose.model('MedicalHistory', medicalHistorySchema);