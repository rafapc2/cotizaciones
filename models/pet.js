var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var petSchema = new Schema({
    name: { type: String, required: [true, 'El nombre es necesario'] },
    gender: { type: String, required: [true, 'El género es necesario'] },
    birthdate: { type: Date },
    weight: { type: Number },
    breed: { type: String },
    img: { type: String, required: false },
});

module.exports = mongoose.model('Pet', petSchema);