//requires
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
//inicializar variables
var app = express();

//cors
app.use(cors({ origin: true, credentials: true }));

//body-parser-config
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


//importar rutas
var appRoutes = require('./routes/app');
var cotizacionesRoutes = require('./routes/cotizaciones');


//rutas
app.use('/', appRoutes);
app.use('/cotizaciones', cotizacionesRoutes);


//escuchar peticiones
app.listen(3000, () => {
    console.log('Express server puerto 3000: \x1b[32m%s\x1b[0m', 'online');
});